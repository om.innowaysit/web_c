@extends('layouts.app')
@section('content')
<div class="max-w-4xl py-10 mx-auto sm:px-6 lg:px-8">
    <div class="mt-5 md:mt-0 md:col-span-2">
        <form method="post" enctype="multipart/form-data" action="{{ route('emp.store') }}">
            @csrf
            <div class="overflow-hidden shadow sm:rounded-md">
                <div class="px-4 py-5 bg-white sm:p-6">
                    <label for="title" class="block text-sm font-medium text-gray-700">Employee Name</label>
                    <input type="text" name="category_name" id="title" type="text" class="block w-full mt-1 text-center text-black border-2 border-gray-800 rounded-md shadow-sm form-input"
                           value="{{ old('title', '') }}" />
                    @error('title')
                        <p class="text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>
                <div class="px-4 py-5 bg-white sm:p-6">
                    <label for="title" class="block text-sm font-medium text-gray-700"> Select an Image</label>
                <input type="file" name="photo" placeholder="Choose image" id="image">
                @error('image')
                    <div class="mt-1 mb-1 alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                <div class="px-4 py-5 bg-white sm:p-6">
                    <label for="title" class="block text-sm font-medium text-gray-700">Gender</label>
                <select class="text-black form-control" name="status">
                        <option class="text-black" value="1" >Male </option>
                        <option class="text-black" value="2" >FeMale </option>
                        <option class="text-black" value="3" >Not To Mention </option>


                </select>
            </div>
                <div class="px-4 py-5 bg-white sm:p-6">
                    <label for="title" class="block text-sm font-medium text-gray-700">Status</label>
                <select class="text-black form-control" name="status">
                        <option class="text-black" value="1" >Active </option>
                        <option class="text-black" value="2" >In-Active </option>

                </select>
            </div>
            <div class="px-10 py-4">
                <button type="submit" class="bg-green-400 text-gray-700 uppercase font-bold w-full px-2 py-1 rounded-lg">Add Employee</button>
            </div>

            </div>
        </form>
    </div>
</div>
@endsection
